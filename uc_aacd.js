Drupal.behaviors.ucAacx = function() {
    $('.attributes').each(function(){
      uc_aacd_calc($(this).find('select'));
    });
};
function uc_aacd_calc(obj){
    var options = [];
    var attributes = [];
    var aacx_nid = $(obj).parent().parent().parent().parent().find('input[type=submit]').attr('id').replace('edit-submit-','');

    $(obj).parent().parent().parent().find('select').each(function(i){
      attributes[i] = $(this).attr('name').replace('attributes[','').replace(']','');
      $(this).find('option').each(function(){
        if($(this).attr('selected')){
          options[i] = $(this).val();
         }
      });
    });
    $.getJSON(base_url + '/index.php?q=uc_aacd',{'aacx_nid' : aacx_nid, 'attributes[]' : attributes, 'options[]' : options},function(res){
      try{
        var newlistprice = $('#node-' + aacx_nid).find('.uc-price-list').html().replace(/\d+/, res.listprice);
          $('#node-' + aacx_nid).find('.uc-price-list').html(newlistprice);
         }catch(err){

        }
        try{
        var newprice = $('#node-' + aacx_nid).find('.uc-price-sell').html().replace(/\d+/, res.price);
          $('#node-' + aacx_nid).find('.uc-price-sell').html(newprice);
        }catch(err){

        }
        try{
            var newdiscount = $('#node-' + aacx_nid).find('.style-number').html().replace(/\d+/,res.discount);
            $('#node-' + aacx_nid).find('.style-number').html(newdiscount);
        }catch(err){

        }

        $('#uc-product-add-to-cart-form-' + aacx_nid).parents().each(function(){
            if($(this).attr("tagName") == 'TD'){
                try{
                    var newlistprice1 = $(this).find('.uc-price-list_price').text().replace(/\d+/,res.listprice);
                    $(this).find('.uc-price-list_price').text(newlistprice1);
                   }catch(err){

                   }
                   try{
                   var newprice1 = $(this).find('.uc-price-sell_price').text().replace(/\d+/,res.price);
                    $(this).find('.uc-price-sell_price').text(newprice1);
                  }catch(err){

                }
                 try{
                     var newprice2 = $(this).find('.uc-price-display').text().replace(/\d+/,res.price);
                     $(this).find('.uc-price-display').text(newprice2);
                    }catch(err){

                 }
                 try{
                     var newdiscount2 = $(this).find('.views-field-field-item-disc-value').html().replace(/\d+/,res.discount);
                     $(this).find('.views-field-field-item-disc-value').html(newdiscount2);
                    }catch(err){

                  }
             }
       });
    });
}
