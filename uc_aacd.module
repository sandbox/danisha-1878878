<?php

/**
 * @file
 * Implements the discounted price on the product page.
 */

/**
 * Implements hook_theme().
 */
function uc_aacd_theme($existing, $type, $theme, $path) {
  return array(
    'node' => array(
      'arguments' => array('node' => NULL, 'teaser' => FALSE, 'page' => FALSE),
      'template' => 'node-product',
    ),
  );
}
/**
 * Implements hook_init().
 */
function uc_aacd_init() {
  $get = json_encode($_GET);
  $arg = json_encode(arg());
  drupal_add_js("var base_url = '{$GLOBALS["base_url"]}';", "inline");
  drupal_add_js("var _get = new Object({$get});", "inline");
  drupal_add_js("var _arg = new Object({$arg});", "inline");
  drupal_add_js(drupal_get_path('module', 'uc_aacd') . '/uc_aacd.js');
}
/**
 * Implements hook_menu().
 */
function uc_aacd_menu() {
  $items['uc_aacd'] = array(
    'title' => 'Product Adjustments',
    'description' => 'Calculate the price of your product.',
    'page callback' => '_uc_aacd_calculate',
    'access arguments' => array('access content'),
    'weight' => 5,
    'type' => MENU_CALLBACK,
  );
  return $items;
}
/**
 * Implements hook_calculate().
 */
function _uc_aacd_calculate() {
  if (!is_numeric($_REQUEST['aacx_nid'])) {
    return;
  }
  $nid = $_REQUEST['aacx_nid'];
  $attributes = isset($_REQUEST['attributes']) ? $_REQUEST['attributes'] : array();
  $options = isset($_REQUEST['options']) ? $_REQUEST['options'] : array();
  $product = node_load($nid);
  $pprice = (float) round($product->sell_price, 2);
  $plistprice = (float) round($product->list_price, 2);
  $pdiscount = (float) ceil((($plistprice - $pprice) / $plistprice) * 100);
  $default_options = array();
  $product_options = array();
  $role_price = array();
  if (module_exists('uc_price_per_role')) {
    $role_prices = uc_price_per_role_load_option_prices($nid);
  }
  foreach ($options as $k => $oid) {
    $default_options[$k] = db_fetch_object(db_query("SELECT price, listprice FROM {uc_attribute_options} WHERE oid = %d", $oid));
    $product_options[$k] = db_fetch_object(db_query("SELECT price, listprice FROM {uc_product_options} WHERE nid = %d AND oid = %d", $nid, $oid));
    if (module_exists('uc_price_per_role')) {
      $role_price[$k] = uc_price_per_role_find_price($role_prices[$oid]);
    }
  }
  foreach ($product_options as $op) {
    $price += (float) round($op->price, 2);
    $listprice += (float) round($op->listprice, 2);
  }
  if ($role_price[0]) {
    $price = 0;
    foreach ($role_price as $op) {
      $price += (float) round($op, 2);
    }
  }
  $listprice = $plistprice + $listprice;
  $discount_custom = uc_discounts_get_discounted_price_for_product($product);
  $discounts = get_codeless_discounts_for_product_and_quantity($product);
  $discount_price = $discounts[0]->discount_amount;
  $previous_price = $pprice;
  $get_discount = db_fetch_object(db_query("select * from {uc_discounts} where is_active=%d", 1));
  if ($get_discount->is_active == 1) {
    if ($discount_price >= 1) {
      if ($discount_custom) {
        $pprice = $discount_custom;
      }
      $listprice = $previous_price + $price;
      if ($product->sell_price == 0.00 && $discounts) {
        $price = $price - $discount_price;
      }
    }
    elseif ($discount_price < 1) {
      if ($discount_custom) {
        $pprice = ceil($discount_custom);
      }
      $listprice = $previous_price + $price;
      $price = (float) ceil($price - ($price * $discount_price));
    }
  }
  $price = $pprice + $price;
  if ($price < 0) {
    $price = 0;
  }
  $discount = (float) ceil((($listprice - $price) / $listprice) * 100);
  print json_encode(array(
  'pprice' => $pprice,
  'plistprice' => $plistprice,
  'pdiscount' => $pdiscount,
  'price' => $price,
  'listprice' => $listprice,
  'discount' => $discount));
  exit;
}
/**
 * Implements hook_form_alter().
 */
function uc_aacd_form_alter(&$form, $form_state, $form_id) {
  if (strstr($form_id, 'uc_product_add_to_cart_form')) {
    $form['#attributes']['class'] .= ' uc-aacx-cart ';
    if (isset($form['attributes'])) {
      $nid = $form['nid']['#value'];
      $product =& $form['#parameters'][2];
      foreach ($form['attributes'] as $id => $attribute) {
        if (is_array($attribute)) {
          $form['attributes'][$id]['#attributes'] = array('onchange' => 'uc_aacd_calc(this);');
        }
      }
    }
  }
  switch ($form_id) {
    case 'uc_object_options_form':
      $table = '{uc_product_options}';
      $id_type = 'nid';
      $sql_type = db_type_placeholder('int');
      foreach ($form['attributes'] as $attribute) {
        $base_attr = uc_attribute_load($attribute['aid']['#value']);
        if ($base_attr->options) {
          $options = array();
          $aid = $attribute['aid']['#value'];
          $result = db_query("SELECT ao.aid, ao.oid, ao.name, ao.cost AS default_cost, ao.price AS default_price, ao.listprice AS default_listprice, ao.weight AS default_weight, ao.ordering AS default_ordering, po.cost, po.price, po.listprice, po.weight, po.ordering, po.ordering IS NULL AS null_order FROM {uc_attribute_options} AS ao LEFT JOIN $table AS po ON ao.oid = po.oid AND po." . $id_type . " = " . $sql_type . " WHERE aid = %d ORDER BY null_order, po.ordering, default_ordering, ao.name", $form['id']['#value'], $attribute['aid']['#value']);
          while ($option = db_fetch_object($result)) {
            $oid = $option->oid;
            $options[$oid] = '';
            $form['attributes'][$aid]['options'][$oid]['select'] = array(
              '#type' => 'checkbox',
              '#default_value' => $attribute['options'][$oid]['select']['#default_value'],
              '#title' => check_plain($option->name),
            );
            $form['attributes'][$aid]['options'][$oid]['cost'] = array(
              '#type' => 'textfield',
              '#default_value' => uc_store_format_price_field_value(is_null($option->cost) ? $option->default_cost : $option->cost),
              '#size' => 6,
            );
            $form['attributes'][$aid]['options'][$oid]['price'] = array(
              '#type' => 'textfield',
              '#default_value' => uc_store_format_price_field_value(is_null($option->price) ? $option->default_price : $option->price),
              '#size' => 6,
            );
            $form['attributes'][$aid]['options'][$oid]['weight'] = array(
              '#type' => 'textfield',
              '#default_value' => is_null($option->weight) ? $option->default_weight : $option->weight,
              '#size' => 5,
            );
            $form['attributes'][$aid]['options'][$oid]['ordering'] = array(
              '#type' => 'weight',
              '#delta' => 50,
              '#default_value' => is_null($option->ordering) ? $option->default_ordering : $option->ordering,
              '#attributes' => array('class' => 'uc-attribute-option-table-ordering'),
            );
          }
          $form['attributes'][$aid]['default'] = array(
            '#type' => 'radios',
            '#options' => $options,
            '#default_value' => $attribute['default']['#default_value'],
          );
        }
      }
      $form['#submit'][] = 'uc_aacd_uc_object_options_form';
      break;

    case 'uc_attribute_option_form':
      $option = db_fetch_object(db_query("SELECT * FROM {uc_attribute_options} WHERE aid = %d AND oid = %d ORDER BY ordering, name", $form['aid']['#value'], $form['oid']['#value']));
      $form['adjustments']['listprice'] = array(
        '#type' => 'textfield',
        '#title' => t('List price'),
        '#default_value' => uc_store_format_price_field_value($option->listprice),
        '#weight' => 2,
      );
      $form['#submit'][] = 'uc_aacd_uc_attribute_option_form';
      break;

    case 'uc_attribute_options_form':
      $options = db_query("SELECT * FROM {uc_attribute_options} WHERE aid = %d ORDER BY ordering, name", $form['aid']['#value']);
      while ($data = db_fetch_object($options)) {
        $form['options'][$data->oid] = array(
          'name' => array(
            '#value' => check_plain($data->name),
          ),
          'cost' => array(
            '#value' => $data->cost,
          ),
          'price' => array(
            '#value' => $data->price,
          ),
          'listprice' => array(
            '#value' => $data->listprice,
          ),
          'weight' => array(
            '#value' => $data->weight,
          ),
          'ordering' => array(
            '#type' => 'weight',
            '#delta' => 50,
            '#default_value' => $data->ordering,
            '#attributes' => array('class' => 'uc-attribute-option-table-ordering'),
          ),
          'ops' => array(
            '#value' => l(t('edit'),
          'admin/store/attributes/' . $data->aid . '/options/' . $data->oid . '/edit') . ' ' .
            l(t('delete'),
        'admin/store/attributes/' . $data->aid . '/options/' . $data->oid . '/delete'),
          ),
        );
      }
      break;
  }
}
/**
 * Implements hook_uc_object_options_form().
 */
function uc_aacd_uc_object_options_form($form, &$form_state) {
  if ($form_state['values']['type'] == 'product') {
    $attr_table = '{uc_product_attributes}';
    $opt_table = '{uc_product_options}';
    $id = 'nid';
    $sql_type = '%d';
  }
  foreach ($form_state['values']['attributes'] as $attribute) {
    if (isset($attribute['options'])) {
      foreach ($attribute['options'] as $oid => $option) {
        if ($option['select']) {
          db_query("UPDATE $opt_table set listprice = %f WHERE nid = %d AND oid = %d",
                   $option['listprice'], $form_state['values']['id'], $oid);
        }
      }
    }
  }
}
/**
 * Implements hook_uc_attribute_option_form().
 */
function uc_aacd_uc_attribute_option_form($form, &$form_state) {
  db_query("UPDATE {uc_attribute_options} SET listprice = %f WHERE aid = %d AND oid = %d",
    $form_state['values']['listprice'], $form_state['values']['aid'], $form_state['values']['oid']);
}
/**
 * Implements phptemplate_uc_object_options_form().
 */
function phptemplate_uc_object_options_form($form) {
  $header = array(array(
  'data' => '&nbsp;&nbsp;' . t('Options')) + theme('table_select_header_cell'),
    t('Default'),
    t('Cost'),
    t('Price'),
    t('Weight'),
    t('List position'));
  $table_id_num = $tables = 0;
  foreach (element_children($form['attributes']) as $key) {
    $rows = array();
    if (element_children($form['attributes'][$key]['default'])) {
      foreach (element_children($form['attributes'][$key]['default']) as $oid) {
        $row = array(
          drupal_render($form['attributes'][$key]['options'][$oid]['select']),
          drupal_render($form['attributes'][$key]['default'][$oid]),
          drupal_render($form['attributes'][$key]['options'][$oid]['cost']),
          drupal_render($form['attributes'][$key]['options'][$oid]['price']),
          drupal_render($form['attributes'][$key]['options'][$oid]['weight']),
          drupal_render($form['attributes'][$key]['options'][$oid]['ordering']),
        );
        $rows[] = array(
          'data' => $row,
          'class' => 'draggable',
        );
      }
      $table_id = 'uc-attribute-option-table-' . $table_id_num++;
      drupal_add_tabledrag($table_id, 'order', 'sibling', 'uc-attribute-option-table-ordering');
    }
    else {
      $row = array();
      $row[] = array('data' => drupal_render($form['attributes'][$key]['default']), 'colspan' => 7);
      $rows[] = $row;
    }
    if (!count($rows)) {
      $rows[] = array(array(
      'data' => t('This !type does not have any attributes.',
      array(
        '!type' => $form['type']['#value'] == 'product' ? t('product') : t('product class'))),
      'colspan' => 7));
    }
    $output .= theme('table', $header, $rows, array('class' => 'product_attributes', 'id' => $table_id), '<h2>' . drupal_render($form['attributes'][$key]['name']) . '</h2>');
    $tables++;
  }
  if (!$tables) {
    $output .= '<br /><br />';
    if ($form['type']['#value'] == 'product') {
      drupal_set_message(t('This product does not have any attributes.'), 'warning');
    }
    else {
      drupal_set_message(t('This product class does not have any attributes.'), 'warning');
    }
  }
  $output .= drupal_render($form);
  return $output;
}
/**
 * Implements phptemplate_uc_attribute_options_form().
 */
function phptemplate_uc_attribute_options_form(&$form) {
  $header = array(t('Name'),
    t('Default cost'),
    t('Default price'),
    t('Default weight'),
    array(
      'data' => t('List position'),
      'sort' => 'asc'),
    t('Operations'));
  if (count(element_children($form['options'])) > 0) {
    foreach (element_children($form['options']) as $oid) {
      $row = array(
        drupal_render($form['options'][$oid]['name']),
        drupal_render($form['options'][$oid]['cost']),
        drupal_render($form['options'][$oid]['price']),
        drupal_render($form['options'][$oid]['weight']),
        drupal_render($form['options'][$oid]['ordering']),
        drupal_render($form['options'][$oid]['ops']),
      );
      $rows[] = array(
        'data' => $row,
        'class' => 'draggable',
      );
    }
  }
  else {
    $rows[] = array(
      array('data' => t('No options for this attribute have been added yet.'), 'colspan' => 7),
    );
  }
  drupal_add_tabledrag('uc-attribute-option-table', 'order', 'sibling', 'uc-attribute-option-table-ordering');
  $output = theme('table', $header, $rows, array('id' => 'uc-attribute-option-table'));
  $output .= drupal_render($form);
  $output .= l(t('Add an option'), 'admin/store/attributes/' . $form['aid']['#value'] . '/options/add');
  return $output;
}
