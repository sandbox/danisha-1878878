<?php
?>
<div id="node-<?php print $node->nid; ?>" class="node<?php
if ($sticky):
  print ' sticky';
  endif;
if (!$status):
  print ' node-unpublished';
  endif;
?>">

<?php print $picture ?>

<?php if ($page == 0): ?>
  <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
<?php endif; ?>

  <?php if ($submitted): ?>
    <span class="submitted"><?php print $submitted; ?></span>
  <?php endif; ?>

  <div class="content clear-block">
  <?php
            $discounts = get_codeless_discounts_for_product_and_quantity($node);
            $oid = $node->attributes[1]->default_option;
            $select = db_fetch_array(db_query("select price from {uc_product_options} where nid=%d and price<>%d", $node->nid, 0.00000));
   print $node->content['model']['#value'];
   print $node->content['body']['#value'];
   if ($discounts):
   print $node->content['list_price']['#value'];
   endif;
   print $node->content['sell_price']['#value'];
   print $node->content['add_to_cart']['#value']; ?>
  </div>

  <div class="clear-block">
    <div class="meta">
    <?php if ($taxonomy): ?>
      <div class="terms"><?php print $terms ?></div>
    <?php endif;?>
    </div>

    <?php if ($links): ?>
      <div class="links"><?php print $links; ?></div>
    <?php endif; ?>
  </div>
</div>
